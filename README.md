# Meetup Eventry

[Copyleft by Kevin Cole (ubuntourist@hacdc.org) 2018.10.13](LICENSE.md)

This system pulls the next ten events from Meetup and displays them in
the wiki.  The system uses the [MediaWiki External Data
extension](https://www.mediawiki.org/wiki/Extension:External_Data),
the Python program `~/bin/eventry`, the [Meetup API: List Group
Events](https://www.meetup.com/meetup_api/docs/:urlname/events/#list),
and `crontab`.

This code was written for Mediawiki 1.27, PHP 7.0, and Python 3.4.

As of 2020.01.25,  running on MediWiki 1.34, PHP 7.2.24, and Python 3.6.9.

## Background

While the External Data extension can retrieve the data directly from
Meetup, it fails to parse nested JSON objects properly. It "flattens"
the objects, which results in duplicate keys -- i.e. 

    {
     "name": "Microcontroller Monday",
     "venue": {"name": "HacDC"},
     "host":  {"name": "HacDC"}
    }
  
is interpreted as:

    {
     "name": "Microcontroller Monday",
     "name": "HacDC",
     "name": "HacDC"
    }`

Thus, `eventry` was born, to eliminate unnecessary fields. It grew
into a wee beastie that computes the end time for an event (stored
only as a duration in the Meetup API), as well as adding markup for
cancelled events, and a time-stamp to indicate the time of the most
recent update query to Meetup.

## To use

(This presumes you already have a working instance of MediaWiki and
that Python 3.x is installed.)

1. Follow MediaWiki's instructions for installing the [External
   Data](https://www.mediawiki.org/wiki/Extension:External_Data)
   extension
2. Create a `~/bin/` directory and move `eventry.py` into
   `~/bin/eventry`. **NOTE:** Remove the `.py` from the name or alter
   the `crontab` entry to match the name.  Set the permissions so that
   it is executable by the owner.
3. Create a directory to hold the results returned from Meetup.
4. Create a `~/.config/` directory and move `eventry.conf` into
   `~/.config/`. **IMPORTANT:** Edit it to meet your needs. (See the
   the documentation for both the **External Data extension** and the
   **Meetup API** referred to (and linked to) above, as well as the
   comments in the boilerplate configuration provided.
5. Test by running `eventry` from the command line and perusing the
   two result files. If all is well, "Keep Calm and Carry On" to
   the next step.
6. Create a `crontab` entry as shown below (adjusting the file name
   if necessary).
7. Modify the MediaWiki instance's `LocalSettings.php` as shown below.
8. Add stanzas to a page where you would like the listing displayed.
9. Optionally, add `~/bin/` to your `PATH` environment variable, for
   ease of running from the Bash shell.

(Clever monkeys may, of course place the executable elsewhere in a
known path -- altering the `cronjob` entry to suit -- and
additionally, may place the configuration file in `/etc`.)

In the following sections, replace `...` with the appropriate paths
created above.

## crontab entry

    0 1,13 * * * /...path-to-bin.../eventry > /dev/null

***Note:*** The time above is Pacific, and is intended to run at 4:00
AM and PM Eastern.

## LocalSettings.php modifications

    # 2018.10.13 KJC - ExternalData options
    #
    $edgDirectoryPath['data'] = "/...path-to-results...";
    $edgFilePath['events']    = "/...path-to-results.../eventry.json";
    $edgFilePath['log']       = "/...path-to-results.../updated.json";

***Note:*** `data`, `events` and `log` are used in the wiki page entry
below as a mapping to obfuscate the actual directory and file paths
being used.  See the [External Data extension
documentation](https://www.mediawiki.org/wiki/Extension:External_Data).


## wiki page entry

    {{#get_file_data:
    file=events
    |format=JSON
    |data=name=name,link=link,start_date=start_date,start_time=start_time,end_date=end_date,end_time=end_time
    }}
    
    {{#get_file_data:
    file=log
    |format=JSON
    |data=updated=updated
    }}
    
    {| class="wikitable"
    !colspan="2"|Start Time
    !colspan="2"|End Time
    ! Event {{#for_external_table:<nowiki/>
    {{!}}-
    {{!}} {{{start_date}}}
    {{!}} {{{start_time}}}
    {{!}} {{{end_date}}}
    {{!}} {{{end_time}}}
    {{!}} [{{{link}}} {{{name}}}]
    }}
    |}
    <span style="display:inline-block; padding:0 0 0 15em;"><span style="text-decoration:underline;"><em>(Events list last updated: <strong>{{#external_value:updated}}</strong>)</em></span></span>

***Note:*** The blank lines between the "stanzas" above are for
clarity only, and may add unwanted whitespace. I suggest removing
them. However, beware trying to compact the source code beyond that:
The [`smarty`](https://www.smarty.net/docs/en/) syntax used above
doesn't appear to like too much compacting.

## Examples

Examples of `eventry.json` and `updated.json` generated from actual
run of `eventry` are provided with this repository. Also `template.txt`
is a copy of the wiki page entry above and can be pasted as a section
into an existing wiki page.

## Dreamhost

Dreamhost decided to screw with the way `crontab` works, destroying my
original crontab entry without telling me.  Now, cron jobs can only be
entered via the [Dreamhost contol
panel](https://panel.dreamhost.com/index.cgi?tab=advanced&subtab=cron&)

The resulting entry now looks like:

    ###--- BEGIN DREAMHOST BLOCK
    ###--- Changes made to this part of the file WILL be destroyed!
    # meetup
    MAILTO=""
    */60 */12 * * * /usr/local/bin/setlock -n /tmp/cronlock.3783821303.228023 sh -c '/home/.../.local/bin/eventry > /dev/null; '
    ###--- You can make changes below the next line and they will be preserved!
    ###--- END DREAMHOST BLOCK

